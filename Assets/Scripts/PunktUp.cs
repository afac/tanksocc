﻿using UnityEngine;
using System.Collections;

public class PunktUp : MonoBehaviour {

 
    public GameObject punkt;
    public GameObject punkt1;
    public GameObject punkt2;
    public GameObject punkt3;

    private Vector3 pos,pos1,pos2,pos3;

    public float UpDist;

    public AudioClip ClickS;

	
	void Start () {
        pos = punkt.transform.position;
        pos1 = punkt1.transform.position;
        pos2 = punkt2.transform.position;
        pos3 = punkt3.transform.position;
	
	}
	

	void Update () {
       
       
	
	}

    void OnMouseOver()

    {
        
        if (punkt.transform.position.z < 0.01)
        {
            punkt.transform.position += punkt.transform.right * UpDist;

            audio.PlayOneShot(ClickS, 0.7F);

            punkt1.transform.position = pos1;
            punkt2.transform.position = pos2;
            punkt3.transform.position = pos3;
        }
        
    }
}
